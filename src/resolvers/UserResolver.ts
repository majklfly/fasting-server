import {
  Resolver,
  Query,
  Mutation,
  Arg,
  ObjectType,
  Field,
  Ctx,
  UseMiddleware,
} from "type-graphql";
import { hash, compare } from "bcryptjs";
import { User } from "../entity/User";
import { Profile } from "../entity/Profile";
import { Request, Response } from "express";
import { createAccessToken, createRefreshToken } from "../auth";
import { isAuth } from "../middlewares/isAuthMiddleware";
import { FastingRecord } from "../entity/FastingRecord";

interface Context {
  req: Request;
  res: Response;
  payload?: { userId: string };
}

@ObjectType()
class LoginResponse {
  @Field()
  id: number;
  @Field()
  accessToken: string;
  @Field()
  profileId: number;
  @Field()
  email: string;
  @Field()
  profile: Profile;
  @Field()
  fastingRecord: FastingRecord;
}

@Resolver()
export class UserResolver {
  // getting a particular user
  @Query(() => String)
  @UseMiddleware(isAuth)
  bye(@Ctx() { payload }: Context) {
    return `your user id is: ${payload!.userId}`;
  }

  // query to return all users
  @Query(() => [User])
  users() {
    return User.find({ relations: ["profile", "fastingrecord"] });
  }

  @Query(() => User)
  async getUser(@Arg("email") email: string) {
    return User.findOne({
      where: { email },
      relations: ["profile", "fastingrecord"],
    });
  }

  // mutation to register a new user and hash the password
  @Mutation(() => Boolean)
  async register(
    @Arg("email") email: string,
    @Arg("password") password: string
  ) {
    // hashing and salting password with bscryptjs
    const hashedPassword = await hash(password, 12);

    // check if user in the database
    const user = await User.findOne({ where: { email } });
    if (user) {
      throw new Error("Email already registered");
    }

    // inserting new user into the table
    try {
      const profile = Profile.create();
      await profile.save();

      const user = User.create({
        email,
        password: hashedPassword,
        profileId: profile.id,
      });
      await user.save();
    } catch (e) {
      console.log(e);
      return false;
    }

    return true;
  }

  // mutation to login the user
  @Mutation(() => LoginResponse)
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() { res }: Context
  ) {
    // looking for the user in the database TODO: that bloody bastard doesn't want to give me back the fasting records
    const user = await User.findOne({
      where: { email },
      relations: ["profile", "fastingrecord"],
    });
    if (!user) {
      throw new Error("Could not find user");
    }

    // compare inserted password with correct one
    const valid = await compare(password, user.password);
    if (!valid) {
      throw new Error("Incorrect password");
    }

    //successfull login
    res.cookie("token", createRefreshToken(user), {
      httpOnly: true,
    });
    return {
      id: user.id,
      accessToken: createAccessToken(user),
      email: user.email,
      profileId: user.profileId,
      profile: user.profile,
      fastingrecord: user.fastingrecord,
    };
  }
}
