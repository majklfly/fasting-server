import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Int,
  InputType,
  Field,
} from "type-graphql";
import { Profile } from "../entity/Profile";

@InputType()
class ProfileInput {
  @Field(() => Int, { nullable: true })
  id?: number;
  @Field(() => String, { nullable: true })
  firstName?: string | " ";
  @Field(() => String, { nullable: true })
  lastName?: string | " ";
  @Field(() => String, { nullable: true })
  gender?: string | " ";
  @Field(() => String, { nullable: true })
  photo?: string | " ";
  @Field(() => String, { nullable: true })
  startingTime?: string | " ";
  @Field(() => Int, { nullable: true })
  height?: number | 0;
}

@Resolver()
export class ProfileResolver {
  @Query(() => [Profile])
  profiles() {
    return Profile.find();
  }

  @Mutation(() => Boolean)
  async updateProfile(
    @Arg("id", () => Int) id: number,
    @Arg("input", () => ProfileInput) input: ProfileInput
  ) {
    try {
      Profile.update({ id }, input);
      return true;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }

  @Mutation(() => Boolean)
  async updateStartingTime(
    @Arg("id", () => Int) id: number,
    @Arg("startingTime") startingTime: string
  ) {
    // checks if profile exists
    const profile = await Profile.findOne(id);
    if (!profile) {
      throw new Error("Couldn't find the profile");
    }

    // updates the profile
    try {
      Profile.update({ id }, { startingTime });
      return true;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }

  @Mutation(() => Boolean)
  async addLog(@Arg("id", () => Int) id: number) {
    // checks if profile exists
    const profile = await Profile.findOne(id);
    if (!profile) {
      throw new Error("Couldn't find the profile");
    }

    // updates the profile
    try {
      Profile.update({ id }, { logs: profile.logs + 1 });
      return true;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }

  @Mutation(() => Boolean)
  async removeLog(@Arg("id", () => Int) id: number) {
    // checks if profile exists
    const profile = await Profile.findOne(id);
    if (!profile) {
      throw new Error("Couldn't find the profile");
    }

    // updates the profile
    try {
      Profile.update({ id }, { logs: profile.logs - 1 });
      return true;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }

  @Mutation(() => Boolean)
  async addUsedLog(@Arg("id", () => Int) id: number) {
    // checks if profile exists
    const profile = await Profile.findOne(id);
    if (!profile) {
      throw new Error("Couldn't find the profile");
    }

    // updates the profile
    try {
      Profile.update({ id }, { usedlogs: profile.usedlogs + 1 });
      return true;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }

  @Mutation(() => Boolean)
  async removeUsedLog(@Arg("id", () => Int) id: number) {
    // checks if profile exists
    const profile = await Profile.findOne(id);
    if (!profile) {
      throw new Error("Couldn't find the profile");
    }

    // updates the profile
    try {
      Profile.update({ id }, { usedlogs: profile.usedlogs - 1 });
      return true;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }

  @Mutation(() => Profile)
  async getProfile(@Arg("id", () => Int) id: number) {
    try {
      const profile = Profile.findOne(id);
      return profile;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }
}
