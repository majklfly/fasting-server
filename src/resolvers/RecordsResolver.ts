import { Resolver, Query, Arg, Int, Mutation } from "type-graphql";
import { FastingRecord } from "../entity/FastingRecord";
import { User } from "../entity/User";

@Resolver()
export class RecordsResolver {
  @Query(() => [FastingRecord])
  fastingRecords() {
    return FastingRecord.find();
  }

  @Mutation(() => Boolean)
  async deleteFastingRecord(@Arg("id", () => Int) id: number) {
    console.log("test");
    // checks if FastingRecord exists
    const record = await FastingRecord.findOne(id);
    if (!record) {
      throw new Error("The record doesn't exist");
    }

    // deleting the record or showing an error
    try {
      FastingRecord.delete(id);
      return true;
    } catch (e) {
      throw new Error(`${e}`);
      return false;
    }
  }

  @Mutation(() => FastingRecord)
  async createFastingRecord(
    @Arg("id", () => Int) id: number,
    @Arg("date") date: string,
    @Arg("startingTime") startingTime: string,
    @Arg("finishTime") finishTime: string,
    @Arg("efh", () => Int) efh: number,
    @Arg("apples", () => Int) apples: number,
    @Arg("color") color: string
  ) {
    // check if user exists
    const user = await User.findOne(id);
    if (!user) {
      throw new Error("User doesn't exist");
    }

    // create a new record and save in the database
    try {
      const record = FastingRecord.create({
        date,
        startingTime,
        finishTime,
        efh,
        apples,
        color,
        userId: id,
        user,
      });
      await record.save();
      return record;
    } catch (e) {
      throw new Error(`${e}`);
    }
  }
}
