import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";

@ObjectType()
@Entity("profile")
export class Profile extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ default: " " })
  firstName: string;

  @Field()
  @Column({ default: " " })
  lastName: string;

  @Field()
  @Column({ default: " " })
  gender: string;

  @Field()
  @Column({ default: " " })
  photo: string;

  @Field()
  @Column({ default: " " })
  startingTime: string;

  @Field(() => Int)
  @Column({ default: 0 })
  height: number;

  @Field(() => Int)
  @Column({ default: 0 })
  logs: number;

  @Field(() => Int)
  @Column({ default: 0 })
  usedlogs: number;
}
