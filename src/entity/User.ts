import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  JoinColumn,
  OneToOne,
  OneToMany,
} from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";

import { Profile } from "./Profile";
import { FastingRecord } from "./FastingRecord";

@ObjectType()
@Entity("users")
export class User extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  email: string;

  @Column()
  password: string;

  @Field(() => Int)
  @Column()
  profileId: number;

  @Field()
  @OneToOne(() => Profile)
  @JoinColumn()
  profile: Profile;

  @Field(() => [FastingRecord])
  @OneToMany(
    () => FastingRecord,
    (fastingrecord: FastingRecord) => fastingrecord.user,
    { onDelete: "CASCADE", onUpdate: "CASCADE" }
  )
  fastingrecord: FastingRecord[];
}
