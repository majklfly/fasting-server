import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { ObjectType, Field, Int } from "type-graphql";
import { User } from "./User";

@ObjectType()
@Entity("fastingrecord")
export class FastingRecord extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ default: " " })
  date: string;

  @Field()
  @Column({ default: " " })
  startingTime: string;

  @Field()
  @Column({ default: " " })
  finishTime: string;

  @Field(() => Int)
  @Column({ default: 0 })
  efh: number;

  @Field(() => Int)
  @Column({ default: 0 })
  apples: number;

  @Field()
  @Column({ default: "white" })
  color: string;

  @Column()
  userId: number;
  @Field(() => User)
  @JoinColumn({ name: "userId" })
  @ManyToOne(() => User, (user: User) => user.fastingrecord)
  user: User;
}
