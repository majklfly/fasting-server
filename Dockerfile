FROM node:10.13.0-alpine

#create app directory
WORKDIR /usr/app

#install app dependencies
COPY package*.json ./
RUN npm install 
RUN npm install typescript@3.8.3 --dev

#copies the local to the docker container
COPY . .

# compile the typescript
RUN npm run build


COPY ormconfig.json ./dist/
COPY .env ./dist/
WORKDIR /usr/app/dist/

EXPOSE 3000
CMD node src/index.js